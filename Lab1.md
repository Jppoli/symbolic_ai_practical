# A search agent for the pacman game

For this practical work, we will use the pacman framework developed in the context of the [course on Artificial Intelligence at Berkeley](https://inst.eecs.berkeley.edu/~cs188/fa20/)

In particular, this work corresponds to the [Project 1](https://inst.eecs.berkeley.edu/~cs188/fa20/project1/) on search agents.


## Introduction

In this project, your Pacman agent will find paths through his maze world, both to reach a particular location and to collect food efficiently. You will build general search algorithms and apply them to Pacman scenarios.

![Image of Pacman](./Images/maze.png)



## Requirements - Hands on Pacman

For this project, you have to use python.

The code for this project consists of several Python files, some of which you will need to **read and understand** in order to complete the work, and some of which you can ignore. You can download all the code and supporting files as a zip archive [here](https://inst.eecs.berkeley.edu/~cs188/fa20/assets/files/search.zip).


The files you will have to edit are :

+ `search.py`: the file where all your search algorithms will reside
+ `searchAgents.py`: the file where all your search-based agents will reside.


Some files are interested to look at :

+ `pacman.py`: The main file that runs Pacman games. This file describes a Pacman GameState type, which you use in this project.
+ `game.py`: The logic behind how the Pacman world works. This file describes several supporting types like AgentState, Agent, Direction, and Grid.
+ `util.py` : Useful data structures for implementing search algorithms.

The other files can be ignored.


## Welcome to Pacman


After downloading the code, unzipping it, and changing to the directory, you should be able to play a game of Pacman by typing the following at the command line:


`python pacman.py`


Pacman lives in a shiny blue world of twisting corridors and tasty round treats. Navigating this world efficiently will be Pacman's first step in mastering his domain.

The simplest agent in `searchAgents.py` is called the GoWestAgent, which always goes West (a trivial reflex agent). This agent can occasionally win:

`python pacman.py --layout testMaze --pacman GoWestAgent`

But, things get ugly for this agent when turning is required:

`python pacman.py --layout tinyMaze --pacman GoWestAgent`

If Pacman gets stuck, you can exit the game by typing `CTRL-c` into your terminal.

Soon, your agent will solve not only tinyMaze, but any maze you want.

Note that `pacman.py` supports a number of options that can each be expressed in a long way (e.g., --layout) or a short way (e.g., -l). You can see the list of all options and their default values via:

`python pacman.py -h`


## Finding a Fixed Food Dot using Depth First Search

In `searchAgents.py`, you'll find a fully implemented SearchAgent, which plans out a path through Pacman's world and then executes that path step-by-step. 

The search algorithms for formulating a plan are not implemented -- **that's your job**. 


First, test that the SearchAgent is working correctly by running:

`python pacman.py -l tinyMaze -p SearchAgent -a fn=tinyMazeSearch`


The command above tells the SearchAgent to use tinyMazeSearch as its search algorithm, which is implemented in `search.py`. Pacman should navigate the maze successfully.

Now it's time to write full-fledged generic search functions to help Pacman plan routes! Remember that a search node must contain not only a state but also the information necessary to reconstruct the path (plan) which gets to that state.

+ **Important note 1** : All of your search functions need to return a list of actions that will lead the agent from the start to the goal. These actions all have to be legal moves (valid directions, no moving through walls).

+ **Important note 2**: Make sure to use `Stack`, `Queue` and `PriorityQueue` data structures provided to you in `util.py`! These data structure implementations will be convenient to implement the different search strategies.

**Hint**: Each algorithm is very similar. Algorithms for DFS, BFS, UCS, and A* differ only in the details of how the fringe is managed. So, concentrate on getting DFS right and the rest should be relatively straightforward. Indeed, one possible implementation requires only a single generic search method which is configured with an algorithm-specific queuing strategy. 

**Implement the depth-first search (DFS)** algorithm in the `depthFirstSearch` function in `search.py`. To make your algorithm complete, write the graph search version of DFS, which avoids expanding any already visited states.

Your code should quickly find a solution for:

`python pacman.py -l tinyMaze -p SearchAgent`

`python pacman.py -l mediumMaze -p SearchAgent`

`python pacman.py -l bigMaze -z .5 -p SearchAgent`

The Pacman board will show an overlay of the states explored, and the order in which they were explored (brighter red means earlier exploration). Is the exploration order what you would have expected? Does Pacman actually go to all the explored squares on his way to the goal?

## Breadth First Search


**Implement the breadth-first search** (BFS) algorithm in the `breadthFirstSearch` function in `search.py`. Again, write a graph search algorithm that avoids expanding any already visited states. Test your code the same way you did for depth-first search.

`python pacman.py -l mediumMaze -p SearchAgent -a fn=bfs`

`python pacman.py -l bigMaze -p SearchAgent -a fn=bfs -z .5`

Does BFS find a least cost solution? If not, check your implementation.

Hint: If Pacman moves too slowly for you, try the option --frameTime 0.

## Varying the Cost Function

While BFS will find a fewest-actions path to the goal, we might want to find paths that are *best* in other senses. Consider `mediumDottedMaze` and `mediumScaryMaze`.

By changing the cost function, we can encourage Pacman to find different paths. For example, we can charge more for dangerous steps in ghost-ridden areas or less for steps in food-rich areas, and a rational Pacman agent should adjust its behavior in response.

**Implement the uniform-cost graph search** algorithm in the `uniformCostSearch` function in `search.py.` We encourage you to look through `util.py` for some data structures that may be useful in your implementation. You should now observe successful behavior in all three of the following layouts, where the agents below are all UCS agents that differ only in the cost function they use (the agents and cost functions are written for you):

`python pacman.py -l mediumMaze -p SearchAgent -a fn=ucs`

`python pacman.py -l mediumDottedMaze -p StayEastSearchAgent`

`python pacman.py -l mediumScaryMaze -p StayWestSearchAgent`

## A* search

**Implement A* graph** search in the empty function `aStarSearch` in `search.py`. A* takes a heuristic function as an argument. Heuristics take two arguments: a state in the search problem (the main argument), and the problem itself (for reference information). The nullHeuristic heuristic function in `search.py` is a trivial example.

You can test your A* implementation on the original problem of finding a path through a maze to a fixed position using the Manhattan distance heuristic (implemented already as `manhattanHeuristic` in `searchAgents.py`).

`python pacman.py -l bigMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic`

You should see that A* finds the optimal solution slightly faster than uniform cost search (about 549 vs. 620 search nodes expanded in our implementation, but ties in priority may make your numbers differ slightly). What happens on openMaze for the various search strategies?









